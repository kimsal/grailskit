package com.grailskit

import grails.transaction.Transactional
import reactor.spring.context.annotation.Consumer
import reactor.spring.context.annotation.Selector

@Transactional
@Consumer
class TeamService {

    def serviceMethod() {

    }

    @Selector('teamRegistered')
    def teamRegistered(Object data)
    {
        println data
    }
}
