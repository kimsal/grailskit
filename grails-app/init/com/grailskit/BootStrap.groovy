package com.grailskit

import com.grailskit.Role
import com.grailskit.Team
import com.grailskit.User
import com.grailskit.UserRole

class BootStrap {

    def init = { servletContext ->
        initUserStuff()
        initTeamStuff()
    }
    def destroy = {
    }

    def initTeamStuff()
    {
        def t = new Team(name:"team1").save()
        def t2 = new Team(name:"team2").save()
        def t3 = new Team(name:"team3").save()
    }
    def initUserStuff()
    {

        def _roles = ['ROLE_USER','ROLE_ADMIN']
        def roles = [:]
        _roles.each { r->
            roles[r] = Role.findByAuthority(r)
            if(roles[r]==null)
            {
                roles[r] = new Role(authority: r).save()
            }
        }
        if(!User.findByUsername("coreadmin"))
        {
            def u = new User(username:"coreadmin")
            u.email="mgkimsal@gmail.com"
            u.password = "coreadmin"
            u.save()
            UserRole.create(u, roles['ROLE_ADMIN'])
            UserRole.create(u, roles['ROLE_USER'])
        }
        if(!User.findByUsername("user1"))
        {
            def u2 = new User(username:"user1")
            u2.email="mgkimsal+1@gmail.com"
            u2.password = "user1"
            u2.save()
            UserRole.create(u2, roles['ROLE_USER'])
        }

    }
}
