package com.grailskit

import grails.gorm.DetachedCriteria
import groovy.transform.ToString
import org.apache.commons.lang.builder.HashCodeBuilder

@ToString(cache=true, includeNames=true, includePackage=false)
class TeamTenant implements Serializable {

	private static final long serialVersionUID = 1

	Team team
	Tenant tenant

	@Override
	boolean equals(other) {
		if (other instanceof TeamTenant) {
			other.teamId == team?.id && other.tenantId == tenant?.id
		}
	}

	@Override
	int hashCode() {
		def builder = new HashCodeBuilder()
		if (team) builder.append(team.id)
		if (tenant) builder.append(tenant.id)
		builder.toHashCode()
	}

	static TeamTenant get(long teamId, long tenantId) {
		criteriaFor(teamId, tenantId).get()
	}

	static boolean exists(long teamId, long tenantId) {
		criteriaFor(teamId, tenantId).count()
	}

	private static DetachedCriteria criteriaFor(long teamId, long tenantId) {
		TeamTenant.where {
			team == Team.load(teamId) &&
			tenant == Tenant.load(tenantId)
		}
	}

	static TeamTenant create(Team team, Tenant tenant) {
		def instance = new TeamTenant(team: team, tenant: tenant)
		instance.save()
		instance
	}

	static boolean remove(Team u, Tenant r) {
		if (u != null && r != null) {
			TeamTenant.where { team == u && tenant == r }.deleteAll()
		}
	}

	static int removeAll(Team u) {
		u == null ? 0 : TeamTenant.where { team == u }.deleteAll()
	}

	static int removeAll(Tenant r) {
		r == null ? 0 : TeamTenant.where { tenant == r }.deleteAll()
	}

	static constraints = {
		tenant validator: { Tenant r, TeamTenant ur ->
			if (ur.team?.id) {
				TeamTenant.withNewSession {
					if (TeamTenant.exists(ur.team.id, r.id)) {
						return ['teamTenant.exists']
					}
				}
			}
		}
	}

	static mapping = {
		id composite: ['team', 'tenant']
		version false
	}
}
