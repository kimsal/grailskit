package com.grailskit

class AuthToken {

	String tokenValue
	String username
	Date dateCreated
	Date lastUpdated
	Integer accessCount = 0

	def afterLoad() {
		accessCount++
	}

	static mapping = {
		version false
	}
}