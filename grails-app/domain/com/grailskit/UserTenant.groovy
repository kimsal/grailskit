package com.grailskit

import grails.gorm.DetachedCriteria
import groovy.transform.ToString
import org.apache.commons.lang.builder.HashCodeBuilder

@ToString(cache=true, includeNames=true, includePackage=false)
class UserTenant implements Serializable {

	private static final long serialVersionUID = 1

	User user
	Tenant tenant

	@Override
	boolean equals(other) {
		if (other instanceof UserTenant) {
			other.userId == user?.id && other.tenantId == tenant?.id
		}
	}

	@Override
	int hashCode() {
		def builder = new HashCodeBuilder()
		if (user) builder.append(user.id)
		if (tenant) builder.append(tenant.id)
		builder.toHashCode()
	}

	static UserTenant get(long userId, long tenantId) {
		criteriaFor(userId, tenantId).get()
	}

	static boolean exists(long userId, long tenantId) {
		criteriaFor(userId, tenantId).count()
	}

	private static DetachedCriteria criteriaFor(long userId, long tenantId) {
		UserTenant.where {
			user == User.load(userId) &&
			tenant == Tenant.load(tenantId)
		}
	}

	static UserTenant create(User user, Tenant tenant) {
		def instance = new UserTenant(user: user, tenant: tenant)
		instance.save()
		instance
	}

	static boolean remove(User u, Tenant r) {
		if (u != null && r != null) {
			UserTenant.where { user == u && tenant == r }.deleteAll()
		}
	}

	static int removeAll(User u) {
		u == null ? 0 : UserTenant.where { user == u }.deleteAll()
	}

	static int removeAll(Tenant r) {
		r == null ? 0 : UserTenant.where { tenant == r }.deleteAll()
	}

	static constraints = {
		tenant validator: { Tenant r, UserTenant ur ->
			if (ur.user?.id) {
				UserTenant.withNewSession {
					if (UserTenant.exists(ur.user.id, r.id)) {
						return ['userTenant.exists']
					}
				}
			}
		}
	}

	static mapping = {
		id composite: ['user', 'tenant']
		version false
	}
}
