

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.grailskit.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.grailskit.UserRole'
grails.plugin.springsecurity.authority.className = 'com.grailskit.Role'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/**',               access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]

grails {
	mail {
		host = '${MAIL_HOST}'
		port = '${MAIL_PORT}'
		username = '${MAIL_PASSWORD}'
		password = '${MAIL_USERNAME}'
		props = ["mail.smtp.auth":"true",
						 "mail.smtp.socketFactory.port":'${MAIL_PORT}',
						 "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
						 "mail.smtp.socketFactory.fallback":"false"]
	}
}
grails.mail.default.from = "from"
grails.mail.default.to = "to"

custom {
	companyName = "Foster Burgess, LLC"
	menu = {
		menu1 = {
			name = "hello"
		}
		menu2 =  {
			name = "hi"
		}
	}
}
core {
	projectName = "demo"
	defaultTitle = "title"
}
grails {
	assets {
		minifyJs = false
		minifyCss = true

	}
}
grails.assets.excludes = [
				"bower_components/**/*",
				"/bower_components/**/*"
]

//println grails