<table class="table table-hover">
    <thead>
         <tr>
             <th width="30"></th>
            <g:each in="${domainProperties}" var="p" status="i">
                <g:set var="propTitle">${domainClass.propertyName}.${p.name}.label</g:set>
                <g:sortableColumn property="${p.name}" title="${message(code: propTitle, default: p.naturalName)}" />
            </g:each>
        </tr>
    </thead>
    <tbody>
        <g:each in="${collection}" var="bean" status="i">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                <td ><g:link class="btn btn-info" action="edit" resource="${bean}">edit</g:link></td>
                <g:each in="${domainProperties}" var="p" status="j">
                        <td><f:display bean="${bean}" property="${p.name}"  displayStyle="${displayStyle?:'table'}" /></td>
                </g:each>
            </tr>
        </g:each>
    </tbody>
</table>