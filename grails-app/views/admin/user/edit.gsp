<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="${gspLayout}" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="edit-user" class="content scaffold-edit" role="main">
            <content tag="adminHeader">
                <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            </content>
            <g:hasErrors bean="${this.user}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.user}" method="PUT">
                <g:hiddenField name="version" value="${this.user?.version}" />
                <div class="nav-tabs">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#usertab" data-toggle="tab">Info</a>
                        </li>
                        <li>
                            <a href="#userroles" data-toggle="tab">Roles</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="usertab">
                        <fieldset class="form">
                            <f:all bean="user"/>
                        </fieldset>

                    </div>
                    <div class="tab-pane" id="userroles">

                        <g:each in="${roles?.sort{it.authority}}" var="role">
                            <p>
                            <g:checkBox name="role.${role.id}"
                                value="on"
                                        checked="${userRoles.find{it.role==role}?.role?.id}"
                            ></g:checkBox>
                            <label for="role.${role.id}">
                            ${role.authority}
                            </label>
                            </p>
                        </g:each>
                    </div>
                </div>
                <fieldset class="buttons">
                    <input class="save" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
