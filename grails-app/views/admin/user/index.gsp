<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="${gspLayout}" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>

        <div id="list-user" class="content scaffold-list" role="main">
            <content tag="adminHeader">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            </content>
            <g:form method="post" action="search">
                Search for: <g:textField name="search" value="${search ?: ''}"/>
                <g:if test="${search}">
                <g:submitButton class="btn btn-info btn-sm" name="clearSearch" value="Clear"/>
                </g:if>
                <g:submitButton class="btn btn-info btn-sm" name="searchButton" value="Search"/>
            </g:form>
            <hr/>
            <f:table collection="${userList}"
            properties="${['username','email']}"
            />

            <div class="pagination">
                <g:paginate total="${userCount ?: 0}" />
            </div>
        </div>
    </body>
</html>