<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<g:createLink uri='/'/>">${config.core.projectName}</a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
            <sec:ifNotLoggedIn>
            <form method="post" action="/login/authenticate" class="navbar-form navbar-right">
                <div class="form-group">
                    <input type="text" placeholder="Username" name="username" class="form-control">
                </div>

                <div class="form-group">
                    <input type="password" placeholder="Password" name="password" class="form-control">
                </div>
                <button type="submit" class="btn btn-success">Sign in</button>
            </form>
            </sec:ifNotLoggedIn>
            <sec:ifLoggedIn>
                <div class="navbar-right">
                    <form class="navbar-form" method="post" action="/logout">
                <div class="btn btn-inverse pull-left">Hello <sec:loggedInUserInfo field="username"/></div>
                        <button class="pull-right btn btn-danger">Log out</button>
                    </form>
                </div>
            </sec:ifLoggedIn>
        </div><!--/.navbar-collapse -->
    </div>
</nav>