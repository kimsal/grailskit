<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="${config.core.defaultTitle}"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:stylesheet src="application.css"/>

    <asset:stylesheet src="bootswatch/${theme}/bootstrap.css"/>
    <asset:stylesheet src="${theme}_override.css"/>

    <g:layoutHead/>
    <asset:javascript src="vue/dist/vue.min.js"/>
    <asset:javascript src="vue-resource/dist/vue-resource.min.js"/>

    <script>
<g:applyCodec encodeAs="none">
window.Core = ${core as grails.converters.JSON};
    </g:applyCodec>
    </script>

</head>
<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">${config.core.projectName}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <input type="text" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" class="form-control">
                </div>
                <button type="submit" class="btn btn-success">Sign in</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>





    <div class="container">
        <div class="page-header">
            <h1>Sticky footer with fixed navbar</h1>
        </div>
        <p class="lead">Pin a fixed-height footer to the bottom of the viewport in desktop browsers with this custom HTML and CSS. A fixed navbar has been added with <code>padding-top: 60px;</code> on the <code>body &gt; .container</code>.</p>
        <p>Back to <a href="../sticky-footer">the default sticky footer</a> minus the navbar.</p>
        <pre>
            oudfas
            sdaf
            ;asdf
            asdf
            asdf
            sadf
            ;sadf
            adsf
            sdaf
            asdf
            asdf
            sadf
            asfas
            df
            ;asdf
            asdf
            asdf
            sadf
            ;sadf
            adsf
            sdaf
            asdf
            asdf
            sadf
            asfas
            df

        </pre>
    </div>

    <footer class="navbar-fixed-bottom footer">
        <div class="container">
            <p class="text-muted">Place sticky footer content here.</p>
        </div>
    </footer>








    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>

    <asset:javascript src="application.js"/>
</body>
</html>
