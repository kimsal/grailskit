<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>grailskit</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <asset:stylesheet src="admin.css"/>
    <asset:javascript src="admin.js"/>



    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">G<b>K</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">Grails<b>Kit</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Users</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><g:link namespace="admin" controller="user"><i class="fa fa-search"></i> Users </g:link></li>
                        <li class=""><g:link namespace="admin" controller="user" action="create"><i class="fa fa-plus"></i> Create</g:link></li>
                    </ul>
                </li>

    <li class=" treeview">
        <a href="#">
            <i class="fa fa-users"></i> <span>Roles</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="active"><g:link namespace="admin" controller="role"><i class="fa fa-search"></i> Roles </g:link></li>
            <li class=""><g:link namespace="admin" controller="role" action="create"><i class="fa fa-plus"></i> Create</g:link></li>
        </ul>
    </li>

                <li>
                    <a href="pages/widgets.html">
                        <i class="fa fa-th"></i> <span>Widgets</span>
                        <span class="pull-right-container">
                            <small class="label pull-right bg-green">new</small>
                        </span>
                    </a>
                </li>

                <li class="header">LABELS</li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <g:if test="${flash.message}">
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            ${flash.message}
        </div>
    </g:if>
    <g:if test="${flash.error}">
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            ${flash.error}
        </div>
    </g:if>

    <section class="content-header">
        <g:pageProperty name="page.adminHeader" />
    </section>
    <section class="content">
    <g:layoutBody/>
    </section>
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
    </footer>

</div>
<!-- ./wrapper -->


<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
//    $.widget.bridge('uibutton', $.ui.button);
</script>

<asset:script>
    var loginButtonCaption = "<g:message code='spring.security.ui.login.login'/>";
var cancelButtonCaption = "<g:message code='spring.security.ui.login.cancel'/>";
var loggingYouIn = "<g:message code='spring.security.ui.login.loggingYouIn'/>";
var loggedInAsWithPlaceholder = "<g:message code='spring.security.ui.login.loggedInAs' args='["{0}"]'/>";
</asset:script>
<asset:javascript src='spring-security-ui.js'/>
<s2ui:showFlash/>
<asset:javascript src="adminlte.js"/>

<s2ui:deferredScripts/>


</body>
</html>
