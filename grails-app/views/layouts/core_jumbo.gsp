<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="${config.core.defaultTitle}"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:stylesheet src="application.css"/>

    <asset:stylesheet src="${theme}.css"/>

    <g:layoutHead/>
    <asset:javascript src="bower_components/vue/dist/vue.min.js"/>
    <asset:javascript src="bower_components/vue-resource/dist/vue-resource.min.js"/>

    <script>
        <g:applyCodec encodeAs="none">
        window.Core = ${core as grails.converters.JSON};
        </g:applyCodec>
    </script>

</head>

<body>

<g:render template="/shared/nav"/>

<div class="mainbody">


<div class="container">
    %{--<div class="jumbotron">--}%
    %{--<div class="container">--}%
    <g:render template="${jumboFile}"/>
    %{--</div>--}%
    %{--</div>--}%
    <g:layoutBody/>

</div> <!-- /container -->
<footer class="navbar-fixed-bottom footer">
    <div class="container">
        <p class="text-muted">&copy; ${new Date().format("YYYY")} ${config.custom.companyName}</p>
    </div>
</footer>

</div>


<div id="spinner" class="spinner" style="display:none;">
    <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>

<asset:javascript src="application.js"/>
</body>

</html>
