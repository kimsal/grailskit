<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
</head>

<body>
<h2>hello</h2>
<div id="info">
New team: <input type="text" v-model="teamName"/>
    <button v-on:click="post">save</button>
</div>
<hr/>
<div id="teams">
    <h3>Teams</h3>
    <li v-for="team in teams">
        {{ team.name }}
    </li>
</div>


<script>
    Vue.http.options.emulateHTTP = true;

    function team(data)
    {
        var self = this;
        self.name = data.name;
    };

    var teams = new Vue({
       el: "#teams",
        data: {
           teams:[]
        },
        methods: {
           init: function()
           {
               var self = this;
               this.$http.get('/team?max=333').then((response) => {
                   console.log(response.data);
                   self.teams = response.data;
               }, (response) => {
               // error callback
           });
           }
        }
    });
    var vue = new Vue({
        el: '#info',
        data: {
            teamName:null,
            team1:null,
            totalItems: 2,
            num:3
        },
        methods: {
            post: function()
            {
                var self = this;
                var _team = this.$resource('/team');
                _team.save({name:self.teamName}).then((response) => {
                    var t = new team(response.data);
                    teams.$data.teams.push(t);
                    self.teamName = null;
                });

            },
            run: function()
            {
                var self = this;
                var team = this.$resource('/team/{id}');
                team.get({id: 1}).then((response) => {
                    self.team1 = response.data;
                });
            },
            fetchData: function(){
                var self = this;
                $.ajax({
                    url: '/team/',
                    type: 'get',
                    dataType: 'json',
                    async: true,
                    success: function (data) {
                        self.num=data.length;
                    }
                });
            }
        }
    });

    teams.init();
</script>
</body>
</html>