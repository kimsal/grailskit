package com.grailskit.admin


class AdminInterceptor {

    AdminInterceptor() {
        match namespace: "admin"
    }

    boolean before() { true }

    boolean after() {
//        if (model == null) {
//            model = [:]
//        }
        model?.gspLayout = "admin"
        true
    }

    void afterView() {
        // no-op
    }
}
