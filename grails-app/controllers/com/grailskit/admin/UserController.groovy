package com.grailskit

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserController {

	static namespace="admin"

	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		def ul = User.createCriteria().list(max:params.max, offset: params.offset ?: 0) {
			if(session.userSearch)
			{
				or {
					ilike('username','%'+session.userSearch+'%')
					ilike('email','%'+session.userSearch+'%')
				}
			}
		}
		respond ul, model:[search:session?.userSearch ?: null, userCount: ul.totalCount]
	}

	def show(User user) {
		respond user
	}

	def create() {
		respond new User(params), model:[userRoles:[], roles:Role.findAll()]
	}

	@Transactional
	def save(User user) {
		if (user == null) {
			transactionStatus.setRollbackOnly()
			notFound()
			return
		}

		if (user.hasErrors()) {
			transactionStatus.setRollbackOnly()
			respond user.errors, view:'create'
			return
		}

		user.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), user.username])
				redirect action:"index", method:"GET"
			}
			'*' { respond user, [status: CREATED] }
		}
	}

	def edit(User user) {
		def userRoles = UserRole.findAllByUser(user)
		respond user, model:[userRoles:userRoles, roles:Role.findAll()]
	}

	@Transactional
	def update(User user) {
		if (user == null) {
			transactionStatus.setRollbackOnly()
			notFound()
			return
		}

		def oldpassword
		if(user.password=="" || user.password==null) {
			 oldpassword = user.getPersistentValue('password')
		}
		user.clearErrors()
		// we might not have a password passed in, so
		// 1. don't change previous password
		// don't validate if a password is passed in or not
		def props = User.getConstrainedProperties().collect{ it->
			it.key!="password" ? it : null
		}
		if(oldpassword)
		{
			user.password = oldpassword
		}
		user.validate(props)

		UserRole.removeAll(user)
		params.role.each { it->
			if(it.value=="on"){
				UserRole.create(user, Role.get(it.key?.toLong()))
			}
		}

		if (user.hasErrors()) {
			transactionStatus.setRollbackOnly()
			respond user.errors, view:'edit'
			return
		}

		user.save flush:true


		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), user.id])
				redirect action:"index", method:"GET"
			}
			'*'{ respond user, [status: OK] }
		}
	}

	@Transactional
	def delete(User user) {

		if (user == null) {
			transactionStatus.setRollbackOnly()
			notFound()
			return
		}

		user.delete flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), user.id])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}

	protected void notFound() {
		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}

	def search()
	{
		if(params.clearSearch)
		{
			session.userSearch = null
		} else {
			session.userSearch = params.search
		}
		redirect(action:"index")
	}
}
