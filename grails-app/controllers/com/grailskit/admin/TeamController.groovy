package com.grailskit.admin

import com.grailskit.Team
import grails.rest.RestfulController

class TeamController extends RestfulController {

    static namespace="admin"

    static allowedMethods = [index: "GET", show: "GET"]

    static responseFormats = ['json', 'xml']

    TeamController()
    {
        super(Team)
    }

    def index() {
        super.index()
    }

    def show() {
        super.show()
    }

    def save() {
        super.save()
    }

    def update() {
        super.update()
    }

    def delete() {
        super.delete()
    }

    Object patch() {
        return super.patch()
    }

    def list() { }



}
