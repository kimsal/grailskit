package com.grailskit.admin

class AdminController {

    static namespace="admin"

    def index() {
        render(view:"index")
    }
}
