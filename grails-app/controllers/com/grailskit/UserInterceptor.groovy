package com.grailskit


class UserInterceptor {

    boolean before() { true }

    boolean after() {
        true
    }

    void afterView() {
        // no-op
    }
}
