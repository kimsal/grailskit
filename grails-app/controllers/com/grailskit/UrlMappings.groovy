package com.grailskit

class UrlMappings {

    static mappings = {

        "/admin"(namespace:"admin"){
            gspLayout = "admin"
            controller="admin"
            constraints {
                // apply constraints here
            }
        }

        "/admin/$controller/$action?/$id?(.$format)?"(namespace:"admin"){
            gspLayout = "admin"
            constraints {
                // apply constraints here
            }
        }


        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }



        "/"(controller:"main", action:"index")
        "/about"(controller:"main", action:"about")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
