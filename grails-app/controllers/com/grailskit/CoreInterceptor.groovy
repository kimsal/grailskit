package com.grailskit

import grails.plugin.springsecurity.SpringSecurityService


class CoreInterceptor {

    SpringSecurityService springSecurityService

    Map core = [:]

    CoreInterceptor() {
        matchAll()
    }

    boolean before() {
        println "B4 "+System.currentTimeMillis()
        true
    }

    boolean after() {
        log.info("AFTER INTERCEPTOR")
        if(session.theme==null || session.theme=="")
        {
            session.theme = "flatly"
        }
        core.user = springSecurityService.principal ? springSecurityService.principal : null
        if(model) {
            model.core = core
        } else {
            model = [:]
        }
        model.theme = session.theme
        model.config = grailsApplication.config
        if(model.gspLayout == null)
        {
            model.gspLayout = "core"
        }
        if(params.gspLayout)
        {
            model.gspLayout = params.gspLayout
        }
//        model.gspLayout = "core"
        true
    }

    void afterView() {
        println "AV "+System.currentTimeMillis()

        // no-op
    }
}
