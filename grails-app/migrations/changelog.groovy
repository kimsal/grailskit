databaseChangeLog = {

    changeSet(author: "michael (generated)", id: "1485749111077-1") {
        createTable(tableName: "auth_token") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "access_count", type: "INT") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "token_value", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "username", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-3") {
        createTable(tableName: "registration_code") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "token", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "username", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-4") {
        createTable(tableName: "role") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "authority", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-5") {
        createTable(tableName: "team") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-6") {
        createTable(tableName: "team_tenant") {
            column(name: "team_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "tenant_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-7") {
        createTable(tableName: "tenant") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-8") {
        createTable(tableName: "user") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "account_expired", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "account_locked", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "email", type: "VARCHAR(255)")

            column(name: "enabled", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "password", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "password_expired", type: "BIT(1)") {
                constraints(nullable: "false")
            }

            column(name: "username", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-9") {
        createTable(tableName: "user_role") {
            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "role_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-10") {
        createTable(tableName: "user_team") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "role_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "team_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-11") {
        createTable(tableName: "user_tenant") {
            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "tenant_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-12") {
        addPrimaryKey(columnNames: "team_id, tenant_id", constraintName: "PRIMARY", tableName: "team_tenant")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-13") {
        addPrimaryKey(columnNames: "user_id, role_id", constraintName: "PRIMARY", tableName: "user_role")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-14") {
        addPrimaryKey(columnNames: "user_id, tenant_id", constraintName: "PRIMARY", tableName: "user_tenant")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-15") {
        addUniqueConstraint(columnNames: "authority", constraintName: "UK_irsamgnera6angm0prq1kemt2", tableName: "role")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-16") {
        addUniqueConstraint(columnNames: "email", constraintName: "UK_ob8kqyqqgmefl0aco34akdtpe", tableName: "user")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-17") {
        addUniqueConstraint(columnNames: "username", constraintName: "UK_sb8bbouer5wak8vyiiy4pf2bx", tableName: "user")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-18") {
        createIndex(indexName: "FK1dw1bh4y7nexm4w0oe00gduk1", tableName: "user_team") {
            column(name: "role_id")
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-19") {
        createIndex(indexName: "FK6d6agqknw564xtsa91d3259wu", tableName: "user_team") {
            column(name: "team_id")
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-20") {
        createIndex(indexName: "FK7t9updw67ti7jwtqg2tu0luh8", tableName: "user_tenant") {
            column(name: "tenant_id")
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-21") {
        createIndex(indexName: "FKa68196081fvovjhkek5m97n3y", tableName: "user_role") {
            column(name: "role_id")
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-22") {
        createIndex(indexName: "FKd6um0sk8hyytfq7oalt5a4nph", tableName: "user_team") {
            column(name: "user_id")
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-23") {
        createIndex(indexName: "FKp8b7j1hxw1hlg15yl3ipq296e", tableName: "team_tenant") {
            column(name: "tenant_id")
        }
    }

    changeSet(author: "michael (generated)", id: "1485749111077-24") {
        addForeignKeyConstraint(baseColumnNames: "role_id", baseTableName: "user_team", constraintName: "FK1dw1bh4y7nexm4w0oe00gduk1", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "role")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-25") {
        addForeignKeyConstraint(baseColumnNames: "team_id", baseTableName: "user_team", constraintName: "FK6d6agqknw564xtsa91d3259wu", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "team")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-26") {
        addForeignKeyConstraint(baseColumnNames: "tenant_id", baseTableName: "user_tenant", constraintName: "FK7t9updw67ti7jwtqg2tu0luh8", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "tenant")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-27") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_role", constraintName: "FK859n2jvi8ivhui0rl0esws6o", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-28") {
        addForeignKeyConstraint(baseColumnNames: "role_id", baseTableName: "user_role", constraintName: "FKa68196081fvovjhkek5m97n3y", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "role")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-29") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_team", constraintName: "FKd6um0sk8hyytfq7oalt5a4nph", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-30") {
        addForeignKeyConstraint(baseColumnNames: "team_id", baseTableName: "team_tenant", constraintName: "FKdabxikjeqw96d5smckun9tnw5", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "team")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-31") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_tenant", constraintName: "FKh767t3kk4bfp5dm6k2ki119ro", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "michael (generated)", id: "1485749111077-32") {
        addForeignKeyConstraint(baseColumnNames: "tenant_id", baseTableName: "team_tenant", constraintName: "FKp8b7j1hxw1hlg15yl3ipq296e", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "tenant")
    }
}
